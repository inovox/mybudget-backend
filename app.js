const express = require('express')
const app = express()
const morgan = require('morgan')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const keys = require('./keys');

// rotas
const indexRoutes = require('./api/routes/index');
const origensRoutes = require('./api/routes/origens');
const centrosCustoRoutes = require('./api/routes/centroscusto')
const usuariosRoutes = require('./api/routes/usuarios')
const lancamentosRoutes = require('./api/routes/lancamento')
const orcamentosRoutes = require('./api/routes/orcamento')
const relatoriosRoutes = require('./api/routes/relatorio')

// conexão ao mongodb
mongoose.connect(
    'mongodb+srv://' + keys.mongodb.user + ':' + keys.mongodb.pwd + '@cluster0.pkwhl.mongodb.net/' + keys.mongodb.databasename + '?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
)

// logar requisições na console
app.use(morgan('dev'))

// parse do body das requisições
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

// CORS
app.use((req,res,next) => {
    res.header('Access-Control-Allow-Origin','*')
    res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization')

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods','PUT, POST, PATCH, DELETE, GET')
        return res.status(200).json({})
    }
    next()
})

// direcionamento de rotas
app.use('/', indexRoutes)
app.use('/origens', origensRoutes)
app.use('/centroscusto', centrosCustoRoutes)
app.use('/usuarios', usuariosRoutes)
app.use('/lancamentos', lancamentosRoutes)
app.use('/orcamentos', orcamentosRoutes)
app.use('/relatorios', relatoriosRoutes)

// retorno de erro para rotas não previstas
app.use((req,res,next) => {
    const error = new Error('Rota não localizada.')
    error.status = 404
    next(error)
})

// retorno para erros em geral disparados pela aplicação
app.use((error,req,res,next) => {
    res.status(error.status || 500)
    res.json({
        error: {
            message: error.message
        }
    })
})

module.exports = app