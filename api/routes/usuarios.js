const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const keys = require('../../keys');

const Usuario = require('../models/usuario')

// criar novo usuário
router.post('/signup', (req, res, next) => {
    const email = req.body.email
    const nome = req.body.nome
    const password = req.body.password

    // valida se registro já existe no banco de dados
    Usuario.find({
            email: email
        })
        .exec()
        .then(resultado => {
            if (resultado.length > 0) {
                res.status(400).json({
                    mensagemerro: 'Já existe um registro com email ' + email + '.'
                })
            } else {
                // cria hash da senha do usuário
                bcrypt.hash(password, 10, (err, hash) => {
                    if (err) {
                        // houve erro ao realizar hash da senha
                        return res.status(500).json({
                            mensagemerro: err.message
                        })
                    } else {
                        // não houve erro no hash, instancia o usuário com a senha "hasheada"
                        const user = new Usuario({
                            email,
                            nome,
                            password: hash
                        })

                        // grava usuário no banco de dados
                        user.save()
                            .then(r => {
                                res.status(201).json({
                                    mensagem: 'Usuário criado com sucesso. Realize login para continuar.',
                                    usuarioCriado: {
                                        email: user.email,
                                        nome: user.nome
                                    }
                                })
                            })
                            .catch(err => {
                                console.log(err)
                                res.status(500).json({
                                    mensagemerro: err.message
                                })
                            })
                    }
                })
            }
        })
        .catch(err => {
            res.status(500).json({
                mensagemerro: err.message
            })
        })
})

// logon de usuário
router.post('/login', (req, res, next) => {
    // valida se registro já existe no banco de dados
    Usuario.find({
            email: req.body.email
        })
        .exec()
        .then(resultado => {
            if (resultado.length < 1) {
                return res.status(401).json({
                    mensagemerro: 'Falha na autenticação. (1)'
                })
            } else {
                bcrypt.compare(req.body.password, resultado[0].password, (err, result) => {
                    if (err) {
                        return res.status(401).json({
                            mensagemerro: 'Falha na autenticação. (2)'
                        })
                    }

                    if (result) {
                        const token = jwt.sign({
                                email: resultado[0].email,
                                nome: resultado[0].nome,
                                id: resultado[0]._id
                            },
                            keys.authPrivateKey, {
                                expiresIn: "12h"
                            }
                        )
                        return res.status(200).json({
                            mensagem: 'Autenticado com sucesso.',
                            token
                        })
                    } else {
                        return res.status(401).json({
                            mensagemerro: 'Falha na autenticação. (3)'
                        })
                    }
                })
            }
        })
        .catch(err => {
            res.status(500).json({
                mensagemerro: err.message
            })
        })
})

module.exports = router;