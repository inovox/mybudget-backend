const express = require('express')
const router = express.Router()
const checkAuth = require('../middlewares/check-auth')

const Lancamento = require('../models/lancamento')
const Origem = require('../models/origem')
const CentroCusto = require('../models/centrocusto')
const Orcamento = require('../models/orcamento')

router.get('/', checkAuth, (req, res, next) => {
    const user = req.dadosUsuario.id

    // recupera registros no banco
    Lancamento.find({
        user
    })
        .select('_id orcamento origem centroCusto data valor descricao')
        .populate('orcamento origem centroCusto')
        .exec()
        .then(resultado => {
            resposta = {
                quantidade: resultado.length,
                registros: resultado
            }
            res.status(200).json(resposta)
        })
        .catch(err => {
            res.status(500).json({
                mensagemerro: err.message
            })
        })
})

router.post('/', checkAuth, (req, res, next) => {
    const user = req.dadosUsuario.id
    const orcamento = req.body.orcamento
    const origem = req.body.origem
    const centroCusto = req.body.centroCusto
    const data = req.body.data
    const valor = req.body.valor
    const descricao = req.body.descricao

    // instancia um novo registro
    const lancamento = new Lancamento({
        orcamento,
        origem,
        centroCusto,
        data,
        valor,
        descricao,
        user
    })

    // persiste novo registro
    lancamento
        .save()
        .then(r => {
            res.status(201).json({
                mensagem: 'Lançamento registrado com sucesso.',
                lancamentoCriada: lancamento
            })
        })
        .catch(err => {
            console.log('erro na criacao', err)
            res.status(500).json({
                mensagemerro: err.message
            })
        })

})

router.delete('/:id', checkAuth, (req, res, next) => {
    const user = req.dadosUsuario.id
    const id = req.params.id

    // valida se registro existe no banco de dados
    Lancamento.find({
        user,
        _id: id
    })
        .exec()
        .then(resultado => {
            if (resultado.length == 0) {
                res.status(404).json({
                    mensagemerro: 'Não foi localizado um registro com id ' + id + '.'
                })
            } else {
                // remove registro 
                Lancamento.deleteOne({
                    user,
                    _id: id
                })
                    .exec()
                    .then(r => {
                        res.status(200).json({
                            mensagem: 'Registro excluído com sucesso.'
                        })
                    })
                    .catch(err => {
                        res.status(500).json({
                            mensagemerro: err.message
                        })
                    })
            }
        })
        .catch(err => {
            res.status(500).json({
                mensagemerro: err.message
            })
        })
})

module.exports = router;