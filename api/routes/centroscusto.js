const express = require('express')
const router = express.Router()
const checkAuth = require('../middlewares/check-auth')

const CentroCusto = require('../models/centrocusto')
const Lancamento = require('../models/lancamento')

router.get('/', checkAuth, (req, res, next) => {
    const user = req.dadosUsuario.id

    // recupera registros no banco
    CentroCusto.find({
        user
    })
        .select('_id descricao')
        .exec()
        .then(resultado => {
            resposta = {
                quantidade: resultado.length,
                registros: resultado
            }
            res.status(200).json(resposta)
        })
        .catch(err => {
            res.status(500).json({
                mensagemerro: err.message
            })
        })
})

router.post('/', checkAuth, (req, res, next) => {
    const user = req.dadosUsuario.id
    const descricao = req.body.descricao

    // valida se registro já existe no banco de dados
    CentroCusto.find({
        user,
        descricao
    })
        .exec()
        .then(resultado => {
            if (resultado.length > 0) {
                res.status(400).json({
                    mensagemerro: 'Já existe um centro de custo ' + descricao + '.'
                })
            } else {
                // instancia um novo registro
                const centrocusto = new CentroCusto({
                    descricao,
                    user
                })

                // persiste novo registro
                centrocusto
                    .save()
                    .then(r => {
                        res.status(201).json({
                            mensagem: 'Centro de custo ' + descricao + ' criado com sucesso.',
                            centrocustoCriada: centrocusto
                        })
                    })
                    .catch(err => {
                        res.status(500).json({
                            mensagemerro: err.message
                        })
                    })
            }
        })
        .catch(err => {
            res.status(500).json({
                mensagemerro: err.message
            })
        })
})

router.get('/:id', checkAuth, (req, res, next) => {
    const user = req.dadosUsuario.id
    const id = req.params.id

    // busca registro no banco de dados
    CentroCusto.find({
        user,
        _id: id
    })
        .exec()
        .then(resultado => {
            if (resultado.length > 0)
                res.status(200).json(resultado)
            else
                res.status(404).json({
                    mensagemerro: 'Não foi localizado um registro com id ' + id + '.'
                })
        })
        .catch(err => {
            res.status(500).json({
                mensagemerro: err.message
            })
        })
})

router.patch('/:id', checkAuth, (req, res, next) => {
    const user = req.dadosUsuario.id
    const id = req.params.id

    // valida se registro existe no banco de dados
    CentroCusto.find({
        user,
        _id: id
    })
        .exec()
        .then(resultado => {
            if (resultado.length == 0) {
                res.status(404).json({
                    mensagemerro: 'Não foi localizado um registro com id ' + id + '.'
                })
            } else {
                // atualiza registro
                CentroCusto.updateOne({ _id: id }, { $set: { descricao: req.body.descricao } })
                    .exec()
                    .then(r => {
                        res.status(200).json({
                            mensagem: 'Registro atualizado com sucesso.'
                        })
                    })
                    .catch(err => {
                        res.status(500).json({
                            mensagemerro: err.message
                        })
                    })
            }
        })
        .catch(err => {
            res.status(500).json({
                mensagemerro: err.message
            })
        })
})

router.delete('/:id', checkAuth, (req, res, next) => {
    const user = req.dadosUsuario.id
    const id = req.params.id

    // valida se registro existe no banco de dados
    CentroCusto.find({
        user,
        _id: id
    })
        .exec()
        .then(resultado => {
            if (resultado.length == 0) {
                return res.status(404).json({
                    mensagemerro: 'Não foi localizado um registro com id ' + id + '.'
                })
            } else {
                // valida se registro já foi utilizado em algum lançamento
                Lancamento.find({
                    user,
                    centroCusto: id
                })
                    .select()
                    .exec()
                    .then(lancamento => {
                        if (lancamento.length > 0) {
                            return res.status(404).json({
                                mensagemerro: 'Registro já foi utilizado em lançamento(s) e não pode ser excluído.'
                            })
                        } else {
                            // remove registro 
                            CentroCusto.deleteOne({
                                _id: id
                            })
                                .exec()
                                .then(r => {
                                    res.status(200).json({
                                        mensagem: 'Registro excluído com sucesso.'
                                    })
                                })
                                .catch(err => {
                                    res.status(500).json({
                                        mensagemerro: err.message
                                    })
                                })
                        }
                    })
                    .catch(err => {
                        res.status(500).json({
                            mensagemerro: err.message
                        })
                    })
            }
        })
        .catch(err => {
            res.status(500).json({
                mensagemerro: err.message
            })
        })
})


module.exports = router;