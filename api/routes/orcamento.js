const express = require('express')
const router = express.Router()
const checkAuth = require('../middlewares/check-auth')

const orcamentosController = require('../controllers/orcamentosController')

router.get('/', checkAuth, orcamentosController.getTodosOrcamentos)
router.post('/', checkAuth, orcamentosController.inserirOrcamento)
router.get('/:id', checkAuth, orcamentosController.detalhesOrcamento)
router.patch('/:id', checkAuth, orcamentosController.atualizarOrcamento)
router.delete('/:id', checkAuth, orcamentosController.excluirOrcamento)

module.exports = router;