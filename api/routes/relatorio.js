const express = require('express')
const router = express.Router()
const checkAuth = require('../middlewares/check-auth')

const relatoriosController = require('../controllers/relatoriosController')

router.get('/orcamento', checkAuth, relatoriosController.getOrcamentos)

module.exports = router;