const jwt = require('jsonwebtoken')
const keys = require('../../keys');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1]
        const decoded = jwt.verify(token,keys.authPrivateKey)
        req.dadosUsuario = decoded
        next()
    } catch (error) {
        return res.status(401).json({
            mensagem: 'Falha na autenticação. (4)'
        })
    }
}