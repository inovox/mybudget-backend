const mongoose = require('mongoose')

const orcamentoSchema = mongoose.Schema({
    denominacao: { type: String, required: true },
    valor: { type: Number, required: true },
    
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario', required: true }
})

module.exports = mongoose.model('Orcamento', orcamentoSchema)