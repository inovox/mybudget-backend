const mongoose = require('mongoose')

const origemSchema = mongoose.Schema({
    descricao: { type: String, required: true },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario', required: true }
})

module.exports = mongoose.model('Origem', origemSchema)