const mongoose = require('mongoose')

const lancamentoSchema = mongoose.Schema({
    orcamento: { type: mongoose.Schema.Types.ObjectId, ref: 'Orcamento', required: true },
    origem: { type: mongoose.Schema.Types.ObjectId, ref: 'Origem', required: true },
    centroCusto: { type: mongoose.Schema.Types.ObjectId, ref: 'CentroCusto', required: true },
    data: { type: Date, required: true },
    valor: { type: Number, required: true },
    descricao: { type: String, required: false },
    
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario', required: true }
})

module.exports = mongoose.model('Lancamento', lancamentoSchema)