const Orcamento = require('../models/orcamento')
const Lancamento = require('../models/lancamento')
const lancamento = require('../models/lancamento')

module.exports = {
    getTodosOrcamentos(req, res) {
        const user = req.dadosUsuario.id

        // recupera registros no banco
        Orcamento.find({
            user
        })
            .select('_id denominacao valor')
            .exec()
            .then(resultado => {
                resposta = {
                    quantidade: resultado.length,
                    registros: resultado
                }
                return res.status(200).json(resposta)
            })
            .catch(err => {
                return res.status(500).json({
                    mensagemerro: err.message
                })
            })
    },
    inserirOrcamento(req, res) {
        const user = req.dadosUsuario.id
        const denominacao = req.body.denominacao
        const valor = req.body.valor

        // valida se registro já existe no banco de dados
        Orcamento.find({
            user,
            denominacao
        })
            .exec()
            .then(resultado => {
                if (resultado.length > 0) {
                    return res.status(400).json({
                        mensagemerro: 'Já existe um registro com denominacao ' + denominacao + '.'
                    })
                } else {
                    // instancia um novo registro
                    const orcamento = new Orcamento({
                        denominacao,
                        valor,
                        user
                    })

                    // persiste novo registro
                    orcamento
                        .save()
                        .then(r => {
                            return res.status(201).json({
                                mensagem: 'Orçamento ' + denominacao + ' criado com sucesso.',
                                orcamentoCriado: orcamento
                            })
                        })
                        .catch(err => {
                            console.log('erro na criacao', err)
                            return res.status(500).json({
                                mensagemerro: err.message
                            })
                        })
                }
            })
            .catch(err => {
                return res.status(500).json({
                    mensagemerro: err.message
                })
            })
    },
    detalhesOrcamento(req, res) {
        const user = req.dadosUsuario.id
        const id = req.params.id

        // busca registro no banco de dados
        Orcamento.find({
            user,
            _id: id
        })
            .exec()
            .then(resultado => {
                if (resultado.length > 0)
                    return res.status(200).json(resultado)
                else
                    return res.status(404).json({
                        mensagemerro: 'Não foi localizado um registro com id ' + id + '.'
                    })
            })
            .catch(err => {
                return res.status(500).json({
                    mensagemerro: err.message
                })
            })
    },
    atualizarOrcamento(req, res) {
        const user = req.dadosUsuario.id
        const id = req.params.id

        // valida se registro existe no banco de dados
        Orcamento.find({
            user,
            _id: id
        })
            .exec()
            .then(resultado => {
                if (resultado.length == 0) {
                    res.status(404).json({
                        mensagemerro: 'Não foi localizado um registro com id ' + id + '.'
                    })
                } else {
                    // tratamentno para preservar valor original caso não seja enviado no body da requisição
                    if ((req.body.denominacao !== undefined) && (req.body.denominacao !== null)) {
                        newDenominacao = req.body.denominacao
                    }
                    else {
                        newDenominacao = resultado[0].denominacao
                    }
                    if ((req.body.valor !== undefined) && (req.body.valor !== null)) {
                        newValor = req.body.valor
                    }
                    else {
                        newValor = resultado[0].valor
                    }

                    // atualiza registro
                    Orcamento.updateOne({ _id: id }, {
                        $set: {
                            denominacao: newDenominacao,
                            valor: newValor
                        }
                    })
                        .exec()
                        .then(r => {
                            res.status(200).json({
                                mensagem: 'Registro atualizado com sucesso.'
                            })
                        })
                        .catch(err => {
                            res.status(500).json({
                                mensagemerro: err.message
                            })
                        })
                }
            })
            .catch(err => {
                res.status(500).json({
                    mensagemerro: err.message
                })
            })
    },
    excluirOrcamento(req, res) {
        const user = req.dadosUsuario.id
        const id = req.params.id

        // valida se registro existe no banco de dados
        Orcamento.find({
            user,
            _id: id
        })
            .exec()
            .then(resultado => {
                if (resultado.length == 0) {
                    return res.status(404).json({
                        mensagemerro: 'Não foi localizado um registro com id ' + id + '.'
                    })
                } else {
                    // valida se registro já foi utilizado em algum lançamento
                    Lancamento.find({
                        user,
                        orcamento: id
                    })
                        .select()
                        .exec()
                        .then(lancamento => {
                            if (lancamento.length > 0) {
                                return res.status(404).json({
                                    mensagemerro: 'Registro já foi utilizado em lançamento(s) e não pode ser excluído.'
                                })
                            } else {
                                // remove registro 
                                Orcamento.deleteOne({
                                    _id: id
                                })
                                    .exec()
                                    .then(r => {
                                        res.status(200).json({
                                            mensagem: 'Registro excluído com sucesso.'
                                        })
                                    })
                                    .catch(err => {
                                        res.status(500).json({
                                            mensagemerro: err.message
                                        })
                                    })
                            }
                        })
                        .catch(err => {
                            res.status(500).json({
                                mensagemerro: err.message
                            })
                        })
                }
            })
            .catch(err => {
                res.status(500).json({
                    mensagemerro: err.message
                })
            })
    }
}