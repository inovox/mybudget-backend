const Orcamento = require('../models/orcamento')
const Lancamento = require('../models/lancamento')
const lancamento = require('../models/lancamento')

/*
Nota: a busca dos orçamentos e valores realizados não está da forma mais "performática" possível pois estou lendo todos os orçamentos
e todos os lançamentos, mas foi o melhor que consegui dentro do contexto de execução de duas promisses.
*/

module.exports = {
    getOrcamentos(req, res) {
        const user = req.dadosUsuario.id
        let orcamentos =[]

        // primeiro dia do mês corrente
        let date = new Date();
        let mesAtual =new Date(date.getFullYear(), date.getMonth(), 1)

        // recupera registros no banco
        Orcamento.find({
            user
        })
            .select('_id denominacao valor')
            .exec()
            .then(orc => {
                orcamentos = orc

                resposta = {
                    quantidade: orcamentos.length,
                    registros: []
                }

                return Lancamento.find({
                    user, 
                    data:  { $gte: mesAtual } 
                })
                    .select('_id orcamento valor')
                    .exec()
            })
            .then(lancamentos => {
                if (orcamentos.length > 0) {
                    orcamentos.map(orcamento => {
                        let realizado = 0
                        let saldo = 0

                        if (lancamentos.length > 0) {
                            // percorre todos os lançamentos
                            lancamentos.map(lancamento => {
                                // incrementa valor realizado
                                if (String(lancamento.orcamento._id) == String(orcamento._id)){
                                    realizado += lancamento.valor
                                }
                            })
                        } else {
                            console.log('não tem lctos')
                        }

                        // saldo do orçamento
                        saldo =parseFloat(orcamento.valor - realizado).toFixed(2)

                        // array com orcamentos para o retorno
                        resposta.registros = resposta.registros.concat({
                            _id: orcamento._id,
                            denominacao: orcamento.denominacao,
                            orcado: parseFloat(orcamento.valor).toFixed(2),
                            realizado: parseFloat(realizado).toFixed(2),
                            saldo
                        })

                    })
                }
                return res.status(200).json(resposta)
            })
            .catch(err => {
                return res.status(500).json({
                    mensagemerro: err.message
                })
            })
    },
}