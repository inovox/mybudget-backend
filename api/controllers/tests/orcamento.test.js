const request = require('supertest')
const app = require('../../../app')

describe('Teste dos Orçamentos', () => {
  it('deve retornar status 401 para requisição sem autenticação', async () => {
    const res = await request(app).get('/orcamentos')

    expect(res.statusCode).toEqual(401)
    expect(res.body).toHaveProperty('mensagem')
  })
})