#!/bin/bash

# Get servers list
set -f
string=$PROD_DEPLOY_SERVER
array=(${string//,/ })

# Iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do 
    echo "Deploy project on server ${array[i]}..."
    ssh ubuntu@${array[i]} "cd apps && cd mybudget-backend && git pull origin master && pm2 restart server.js"
done